//// ImageJ macro to interpret atlases.bin obtained with EasyGrid Control (mostly a low-pass filtering)

// 1. Set "gridBar" pixels to a high value => black grid bars after LUT inversion
run("Macro...", "code=[if(isNaN(v)) v=400]");  // !! HARD-CODED value !!

// 2. IJ low-pass filter (requires input in 8-bit)
run("8-bit");
// Filter parameters:
//   suppress=X: supress stripes
//   tolerance=X: tolerance of direction
//   autoscale: autoscale after filtering
//   saturate: saturate image when autoscaling
run("Bandpass Filter...", "filter_large=5000 filter_small=6 suppress=None tolerance=5")

// 3. Adjust display parameters
run("Grays");
run("Invert LUT");
setMinAndMax(130, 160);
