# EasyGrid script repository

Welcome to the code repository of the first EasyGrid paper ([link to preprint/paper coming soon])

Contents:
• Code used to perform FFT-based ice quality quantitation in lamellae of plunge- or jet-vitrified cells   @iceQ/
     The text file "1_iceQprocessing-stepByStep.sh" is more of a readme explaining command lines and scripts mediating the processing ; it won't do much if you run it as is. (I kept it as a .sh file simply for the color-code that makes it easier to read when opening it with `vi`.) Please refer to it to reproduce the analysis.

• Code used to picked particles as Gaussian "blobs" for single-particle analysis of ribosomes   @blobPycking/

Thank you for your interest and please contact Gergely (gpapp@embl.fr) for any question regarding our work.

All the best,
Olivier Gemin (olivier.gemin@protonmail.com)
