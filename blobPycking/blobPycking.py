# Script to pick blobs in TEM images.
# Input image "imgName" declared as argument : python blobPycking.py -n imgName     # ! only the file name without path : Ipath is hard-coded in the script (after fuction definition)
# Hard-coded image processing parameters (see S, lp, hp, SKth below) work well with ribosomes (~300A wide) imaged at 0.731 A/px.
# Outputs : .star files with picked coordinates  (& optional 'quality check' images of the intermediate steps of processing)

# Import libraries
import argparse
import itertools
import math
import numpy as np
import mrcfile # see https://mrcfile.readthedocs.io/
from skimage.measure import block_reduce #scikit-image module
from scipy.ndimage import gaussian_filter
from skimage.morphology import skeletonize_3d

# Read image name provided as argument
parser = argparse.ArgumentParser()
parser.add_argument("-n", "--name", type=str)
args = parser.parse_args()
imgName = args.name
if imgName[-4:] == '.mrc':
    imgName=imgName[:-4]

# Function to clean picks by distance
def distCln(input_list, threshold=100):
    combos = itertools.combinations(input_list, 2)
    points_to_remove = [point2 for point1, point2 in combos if math.dist(point1, point2)<=threshold]
    points_to_keep = [point for point in input_list if point not in points_to_remove]
    return points_to_keep

# Function to format output .star file
def newStar(name):
    with open(name+'_manualpick.star', 'w') as f:
        f.writelines(["\ndata_", '\n', '\nloop_', '\n_rlnCoordinateX #1', '\n_rlnCoordinateY #2',
                      '\n_rlnClassNumber #3', '\n_rlnAnglePsi #4','\n_rlnAutopickFigureOfMerit #5\n'])

##########>> User Input <<##########
S=4 #Shrinkage = binning factor before picking
lp=30/S # bandpass1
hp=80/S # bandpass2
SKth=-0.5 # Skeletonization threshold : adjust it depending on your contrast ; first set QCHKflag to one and measure pixel intensity (particle VS noise) after bandpass filtering to adjust SKth (if negative, higher than particle value to pick it but lower than noise to limit false positive picks)
minDist=80 # distance cleaning in unbinned pixels (removes particles too close to one another)
border=180 # min/max clipping in unbinned pixels (ignore image borders)
Xmax=4096
Ymax=4096

imgPath='allFrames/'
QCHKflag=0
QCHKpath='Qchk/'
#########<< End of Input >>#########

pts = np.zeros((0,2),np.float32) # init list

# Read image
I=imgPath+imgName+".mrc"
with mrcfile.open(I) as mrc: #open
    v=mrc.data #read

# Process image
v   = (v-v.mean())/v.std() # normalize
vv  = block_reduce(v,(S,S,S)) #bin
if QCHKflag==1: #chk?
    mrcfile.write(QCHKpath+imgName+'_S%i.mrc'%(S), np.float32(vv))

vv1  = gaussian_filter(vv,lp) #blur
vv  = vv1 - gaussian_filter(vv,hp) #bandpass
if QCHKflag==1: #chk?
    mrcfile.write(QCHKpath+imgName+'_S%iL%iH%i.mrc'%(S,lp,hp), np.float32(vv))

vvg = skeletonize_3d((vv<SKth))#pick
if QCHKflag==1: #chk?
    mrcfile.write(QCHKpath+imgName+'_S%iL%iH%i%.2f.mrc'%(S,lp,hp,SKth), np.float32(vvg))

tmp = np.argwhere(vvg>0) # get coordinates of all ones. tmp.shape = (N,3)
#pts = np.vstack((pts,tmp)) # list pts as x,y,z coordinates
pts=tmp
pts = S*pts + S + S/2 # Un-shrink coordinates & count pixels starting from 1 (.star convention)
pts=np.array([pts[:,2],pts[:,1]]).transpose() # z,y,x => x,y
pts=pts[pts[:,0]>border] #trim
pts=pts[pts[:,1]>border] #trim
pts=pts[pts[:,0]<Xmax-border] #tram
pts=pts[pts[:,1]<Ymax-border] #trum

clnPts=distCln(pts.tolist(),minDist) #clean by distance

newStar(imgPath+imgName) #init output
with open(imgPath+imgName+'_manualpick.star', 'a') as f: #write
    for row in clnPts:
        f.write(' '.join([str(a) for a in row]) + ' -999 -999 -999\n')

clnPts=np.array(clnPts) #format
np.savetxt(imgPath+imgName+'_S%i.txt'%S,clnPts/S) # also save binned coordinates for visual checking in imod using point2model
print(imgName,clnPts.shape[0]) # Display number of picks per image.
