# Script to pick blobs in tomograms - useful for subtomogram averaging of big macromolecules e.g. ribosomes.
# Convention is black particles, white background. (if not, change vvg = skeletonize_3d((vv<thr))  to  vvg = skeletonize_3d((vv>thr)))
# Hard-coded image processing parameters (see S, g1, g2, thr below) work well with ribosomes (~300A wide) in tomos reconstructed at ~8A/px.
# Warning : tomo name template hard-coded after function definition section !!  Tweak it to your naming convention.
# Outputs : .star files with picked coordinates  (& optional 'quality check' images of the intermediate steps of processing)

##From scratch: first set up environment
#module load Mamba
#mamba init
# Cut new lines added to ~/.bashrc and paste them in ~/mambaInit.sourceMe
#source ~/mamba.sourceMe
#mamba create -n env01
#mamba activate env01
#mamba install numpy scipy scikit-image
#pip install mrcfile

# Import libraries
import glob
import mrcfile
import numpy as np
from scipy.ndimage import gaussian_filter,binary_dilation,binary_erosion,median_filter
from skimage.measure import block_reduce #scikit-image module
from skimage.morphology import skeletonize_3d
import csv
import os

# Tomograms are expected to be named "3-digit-number".mrc
#tomolist=(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81)
tomolist=(2,4,5,6,7,15,16,17,18,19,20,21,22,23,24,25,28,29,38,39,40,47,50,51,52,57,58,59,60,61,64,65,70)

# Hard-coded paths to data, outputFolder and quality-check (QC)filesFolder
tomoPath='tomos/'
outPath='blobPks-WarpRec/'
chkPath='tmp/'

QchkFlag=1 # Faster without generating images (QchkFlag=0) but they help tweaking S, g1, g2 & thr

tomoDim=(512,512,160) # Tomo volume dimensions (here, bin8). All tomos should have the same Z-heigth.

trimVal=30  # XY border trim, in binned pixels
ZtrimVal=30

S = 1  # Shrink factor
g1 = 3 # cutoff 1
g2 = 7 # cutoff 2
thr= -0.55 # Skeletonizing threshold (dark particles => negative.)

clnDistFlag=1 # Clean picks by proximity (strongly advised ! The algorithm will pick particles on several consecutive slices of the tomogram => lots of duplicates before cleaning.)
clnDistThr=8  # Particle radius (px)

modSphSize = 9 # @end, the script suggests command lines to check picking results in IMOD [convert coordinates.txt to a .mod model as a 'scattered' object with picks represented as 3D spheres of radius 'modSphSize']. Modify hard-coded 'modname' to control output names.

##########################################################
# ClnDist function
def filterDist(data,condition):
 result = []
 for element in data:
  if all(condition(element,other) for other in result):
   result.append(element)
 return result

def quad_condition(xs,ys):
 return sum((x-y)*(x-y) for x,y in zip(xs,ys)) > clnDistThr*clnDistThr # proximity condition
##################

#Initialize min/max coordinates
Xmin=trimVal ; Xmax=tomoDim[0]-trimVal
Ymin=trimVal ; Ymax=tomoDim[1]-trimVal
Zmin=ZtrimVal ; Zmax=tomoDim[2]-ZtrimVal

# Initialize picks-list
pts = np.zeros((0,3),np.float32) # Coordinates matrix

for i in tomolist:
		# Open tomogram
        #tomoName = tomoPath+'%03i.bin8.at.mrc'%i #####  FILL IN TOMO_NAME TEMPLATE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< hardcoded user input
        tomoName = tomoPath+'%03i_15.74Apx.mrc'%i 
        with mrcfile.open(tomoName) as mrc: # Read tomogram
            v=mrc.data
		
		# Pick blobs
        v   = (v-v.mean())/v.std() # normalize
        v1   = gaussian_filter(v,g1) # blur
        v   = v1 - gaussian_filter(v,g2) # bandpass
        vv  = block_reduce(v,(S,S,S),np.min) # skimage.measure.block_reduce ~ binning. No effect if S=1.
        vvg = skeletonize_3d((vv<thr)) # Keep strong-signal regions that are below thr, then shrink it to a line/point.
        ccv = gaussian_filter(np.float32(vvg),1) # blur skeleton (for QC image generation)
        pts = np.argwhere(vvg>0) # get coordinates of all ones. tmp.shape = (N,3)

		# Generate QC volumes
        if QchkFlag==1:
            with mrcfile.new(chkPath+'%03i-1-v_g%i-g%i.rec'%(i,g1,g2),overwrite=True) as mrc:
                mrc.set_data(np.float32(v))   # bandpassed tomogram
            with mrcfile.new(chkPath+'%03i-2-vv_blkRed%i.rec'%(i,S),overwrite=True) as mrc:
                mrc.set_data(np.float32(vv))  # bandpassed & shrunk tomogram
            with mrcfile.new(chkPath+'%03i-3-vvg_skel3D%i.rec'%(i,thr),overwrite=True) as mrc:
                mrc.set_data(np.float32(vvg)) # skeletonized signal (picks before cleaning)
            with mrcfile.new(chkPath+'%03i-4-ccv_skel3D-blur1.rec'%i,overwrite=True) as mrc:
                mrc.set_data(np.float32(ccv)) # blurred picks for visual inspection (picks before cleaning)

		# Trim picks & save them as a list of coordinates
        print( 'Tomo %2d: %6d'%(i,pts.shape[0])) # Quick check of #picks per tomogram
        pts = S*pts + S/2 # Un-shrink coordinates and put them back in IMOD convention (start at 1)
        ix  = (pts[:,0]>Zmin) & (pts[:,1]>Ymin) & (pts[:,2]>Xmin) & (pts[:,0]<Zmax) & (pts[:,1]<Ymax) & (pts[:,2]<Xmax) # Trim tomo borders (XZ swapped for some reason)
        pts = pts[ix>0] # Select picks away from border
        print(pts.shape) # Quick check of #picks per tomogram (after border-trimming)
        modname='tomo%i'%i     #####  FILL IN output MOD_NAME TEMPLATE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< hardcoded user input
        pos = np.vstack( (pts[:,2],pts[:,1],pts[:,0]) ).transpose() # Format picked coordinates
        print(modname,pos.shape)
        np.savetxt(outPath+modname+'.txt',pos) # Output picked coordinates

		# Proximity-clean picks & save them as a list of coordinates
        if clnDistFlag==1: # Proximity-cleaning (highly recommended)
            posCLN = np.array(filterDist(pos,quad_condition)) # Cleaning
            print(posCLN.shape) # Quick check of #picks per tomogram (after proximity cleaning)
            pos=posCLN # Overwrite position list
            modname='tomo%i-CLN%i'%(i,clnDistThr) # Initialize QC mod file name
            np.savetxt(outPath+modname+'.txt',pos)# Output picked coordinates (after proximity cleaning)

		# Suggest command lines to check picking results
        print('checkable in IMOD after : point2model -in '+outPath+modname+'.txt -ou '+outPath+modname+'.mod -sc -sp %i'%modSphSize)
        print('3dmod '+chkPath+'%03i-1-v_g%i-g%i.rec '%(i,g1,g2)+outPath+modname+'.mod')
        print('3dmod '+tomoName+' '+outPath+modname+'.mod')
        print('imod '+tomoName)
print('Done!')
