# Suggestions of command lines to prepare & process images for FFT-based crystallinity assessment
# Requires following scripts : fRename.sh | parsePyScript.sh | 2_iceQanalysisTemplate.py | B_py-1gpu8cpu1h.sh (adapted to your cluster architecture and mamba/conda/python environment) | 3_iceQstats.ipynb
# Requires python library mrcfile from https://mrcfile.readthedocs.io/

# #Data : arrays of high-res TEM images on lamellae (~1.19A/px ; 20 e-/A2) on lamellae

# #Pre-processing
#
# - Import & MotionCorrect (5x5) in RELION => .mrc images in MotionCorr/job002/frames/
#
# - Renumber frames to call them easily in following (python) scripts : Npos_Nshot.mrc
mkdir renamedFrames # create output dir
ls MotionCorr/job002/frames/*.mrc > frame.list       # list input frames
awk -v spp=5 '{print int((NR+spp-1)/spp),(NR-1)%spp+1,$1}' frame.list > fRenaming.list # create input list ; hardcoded for 5 shots per position (spp)
./fRename.sh fRenaming.list         # Links data in renamedFrames/ with updated names : Npos_Nshot.mrc
#
# - Compute FFT spectra with IMOD (provides 'more interpretable' contrast than when I calculated it in python)
mkdir renamedPS     # create output dir
ls renamedFrames/*mrc > rF.list         # list input frames
sed -i 's/\.mrc//g' rF.list                             # edit list: rm extension
awk -F/ '{print $NF}' rF.list  > temp; mv temp rF.list  # edit list: rm path
split -n l/4 rF.list # split working list in 4 parts (or more) to parallelize computation in several terminal windows
# Open as many terminals as you see fit. At least 3 more, in this case. #ctrl+shift+n
for sublist in `ls x??` ; do echo "module load IMOD ; for i in \`cat $sublist\` ; do if [ ! -f renamedPS/\$i.ps.mrc ] ; then clip spectrum renamedFrames/\$i.mrc renamedPS/\$i.ps.mrc ; fi ; done" ; done  # print commands to copy paste in the "parallel" terminals

# # H2O frequency analysis 
# - Split python script for parallel processing (hardcoded here for 30 positions per cluster job)
echo "" > temp ; rm pyparse.list   # set stage
for i in `seq 0 100` ; do awk -v var="$i" '{print var*30+1,"\t",(var+1)*30}' temp >> pyparse.list ; done    # create input list
rm temp ; vi pyparse.list  ## MANUAL STEP ## edit last number in list to be your actual N_max_positions
./parsePyScript.sh pyparse.list     # replicate 2_iceQanalysisTemplate.py into parallelizable scripts.py   (fyi: analyzed frequencies are hard-coded in 2_iceQanalysisTemplate.py)
# - PPPP : Pretty Parallel Python Processing
mkdir npOutput figures figures/allFrames-msk810_870-tL-20+EM        # create output dirs
ls *from* > py.list # create input list
for i in `cat py.list` ; do sbatch ./B_py-1gpu8cpu1h.sh $i ; done # launch each job.py on the cluster

# #Results
# - Graphical summaries of H2O analysis figures/allFrames.../ (one plot per frame)
# - Matrix of "H2O peak counts" : npOutput/*.npy

# #Statistics
# - Auto-label positions % number of counts using 3_iceQstats.ipynb
# - Curate results based on visual checks (g=glassy d=dubious=semi-crystalline c=crystalline)
#     & edit 3_iceQstats.ipynb accordingly
cd AutoLabeledFigures--msk810_870-tL-20/c50+ ; eog *png
cd ../d10-50    ; eog *png
cd ../g10-      ; eog *png
#       Re-attribution criteria :
#       g -> d : if ring/peaks not detected @FFT && no obvious crystal @realSpace
#       g -> c : if obvious crystal @realSpace
#       
#       d -> g : if peak detections come from salt&pepper noise in the 2D FFT && no crystal @realSpace
#       d -> c : if obvious crystal @realSpace
#       
#       c -> g : if peak detections come from salt&pepper noise in the 2D FFT && no crystal @realSpace
#       c -> d : (rather subjective, sorry) if peak detection overshot @FFT && no obvious crystal @realSpace
#
# - Get proportions g/d/c for analyzed images in 3_iceQstats.ipynb
# Optional : pool numbers from different grids and output bargraph plunge-frozen VS jet-frozen using 3_iceQstats.ipynb
