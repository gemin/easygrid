# Script to quantify ice crystallinty from TEM images preprocessed with 1_iceQpreprocessing.sh.
# Parameters are hard-coded to suit an acquisition at 1.19 A/px
# User input needed in the last section of the file (after defining necessary functions)
# Outputs: - Graphical summaries.png of H2O analysis in figures/allFrames.../ (one plot per frame)
#          - Matrices of "H2O peak counts" in npOutput/*.npy
# reminder : mkdir output folders before running this script
##########################################################################

# Import libraries
import mrcfile # see https://mrcfile.readthedocs.io/
import numpy as np
import matplotlib.pyplot as plt
import scipy
from skimage.measure import label #scikit-image module

# Create "Water ring mask" (Wmask)
dim=1024
c=[dim//2,dim//2] # circle center 
low=810-c[0]      # lower boundary radius 
high=870-c[1]     # higher boundary radius.  Hardcoded ring thickness : 810 to 870
Wmask=np.zeros((dim,dim)) # Generate mask
for x in np.arange(dim):  # loop on each pixel ; 1 only if in between boundary circles.
    for y in np.arange(dim):
        d=(x-c[0])*(x-c[0])+(y-c[1])*(y-c[1])
        Wmask[x,y]=(d>low*low)&(d<high*high)

##################### Define analysis functions ###########################
########################################################################### hho_mask #
# Function to open & mask file
def hho_mask(I,plotFlag=0): # I: power spectrum to process | plotFlag
    if plotFlag==1: #chk?
        plt.figure(figsize=(1,20))
        plt.imshow(Wmask)
        plt.title("Ice ice baby")
    with mrcfile.open(I) as mrc: #open
        F=mrc.data #read
    fm = F * Wmask #mask
    return fm #output

########################################################################### hho_hist #
# Function to open file, mask & plot it + provide histogram.
def hho_hist(I,pctile,imgFlag=0):   # I: power spectrum to process | pctile: just to check if a hard-coded value or a %ile would perform better at peak detection. Turned out the fixed value -20 worked better in my hands. Now, distribution shape might work even better than peak detection but I went with it and corrected faulty glassy/??/crystalline labeling 'by hand'.
    fm = hho_mask(I) #open,read,mask
    s = fm[fm<0].flatten()    # s=Signal. Ignore zeros: they've been water-masked out. Image has negative values.
    cutoff = np.percentile(s,pctile)
    ################################################################################
    if imgFlag==1: #chk?
        plt.figure()
        plt.subplot(1,2,1) ; plt.imshow(F)
        plt.subplot(1,2,2) ; plt.imshow(fm)
    plt.figure(figsize=(10,2)) ; plt.hist(s,bins=20)
    return fm,cutoff

########################################################################### hho_peaks #
# Function to extract ice peaks.
def hho_peaks(I,tL,tH=0,plotFlag=1,emFlag=0,EMinput='',saveFlag=0,Opath='figures/pathNotSet.png'): # I: power spectrum to process | tL: low fixed threshold for peak detection. -20 worked well in 2/4 datasets. | tH: high threshold to ignore saturated pixels | plotFlag | emFlag: also display raw image to check for crystallinity in the real space ? | EMinput : original image | saveFlag : graphical summary ? | Opath: output path for the optional graphical summary
    ErodElSize=0 ; DilElSize=2 # Define structural element for erosion-dilation. (dim being 2 here, dim,0=dot | dim,1=cross | dim,2=square)
    Ndil=4 # N-1 rounds of dilations (1st one for free == hard-coded.)
    
    ttlFontSize=15 # plot cosmetics
    #############################################
    strEl1 = scipy.ndimage.generate_binary_structure(2,ErodElSize) # structural element for erosion ; hard-coded as 2D dot (2,ErodElSize=0)
    strEl2 = scipy.ndimage.generate_binary_structure(2,DilElSize)  # structural element for dilation; hard-coded as 2D square (2,DilElSize=2)
    
    fm=hho_mask(I) #open,read,mask
    fth=fm>tL # Threshold for peak detection. Selected -20 from H2O_hist(I).
    pks = np.argwhere((fm>tL)&(fm<tH)) # Find indices of peaks

    # Get all peaks (noisy ; multiple hits per peak)
    PKS=np.zeros(fm.shape) #init image
    for i in pks: #grab
        PKS[i[0],i[1]]=1

    # 1x erode to clean out isolated pixels ; 5x dilate to merge neighbors
    epks =scipy.ndimage.binary_erosion(PKS,strEl1)       #erosion
    edpks=scipy.ndimage.binary_dilation(epks,strEl2)    #dilation
    for i in range(Ndil):
        edpks=scipy.ndimage.binary_dilation(edpks,strEl2)    #more dilations. 5 rounds made peak size close to original sizes 

    # Connected-components Analysis : count number of clusters.
    label_image = label(edpks) ; Npks=np.max(label_image)

    ##Plotting##########################################
    dim=1024 ; crop=100 ; c=crop ; C=dim-crop ; r=crop ; R=dim-crop; 
    #c=280;C=480; r=100;R=300; #c=0;C=dim; r=0;R=dim; 
    if plotFlag==1:
        if emFlag==1:
            with mrcfile.open(EMinput) as mrc:
                EM=mrc.data
            plt.figure(figsize=(10,10))
            plt.subplot(2,2,1) ; plt.imshow(EM,cmap='gray') ; plt.title("Raw data",fontsize=ttlFontSize)
            plt.text(0,-400,I)
            plt.subplot(2,2,2) ; plt.imshow(fm[r:R,c:C]) ; plt.title("Masked FFT",fontsize=ttlFontSize)                    
            plt.subplot(2,2,3) ; plt.hist(fm[fm<0].flatten(),bins=20) ; plt.title('Masked FFT instensity',fontsize=ttlFontSize)
            plt.subplot(2,2,4) ; plt.imshow(edpks[r:R,c:C]) ; plt.title("%i detected peaks"%(Npks),fontsize=ttlFontSize)
        else:
            plt.figure(figsize=(10,5))
            plt.subplot(1,2,1) ; plt.imshow(fm[r:R,c:C]) ; plt.title("Masked FFT",fontsize=ttlFontSize)        
            plt.text(0,-90,I)
            plt.subplot(1,2,2) ; plt.imshow(edpks[r:R,c:C]) ; plt.title("Detected peaks ; %ix"%(Ndil+1)+strElLabel[DilElSize],fontsize=ttlFontSize)
        if saveFlag==1:
                plt.savefig(Opath, bbox_inches='tight', dpi=300)
                plt.close()
    if plotFlag==2:
        plt.figure(figsize=(25,15)) ; Nrows=2; Ncols=3
        plt.subplot(Nrows,Ncols,1) ; plt.imshow(PKS[r:R,c:C])
        plt.title('Raw peaks',fontsize=ttlFontSize)
        plt.subplot(Nrows,Ncols,2) ; plt.imshow(gpks[r:R,c:C])
        plt.title('Blurred peaks ; g=%i'%G,fontsize=ttlFontSize)
        plt.subplot(Nrows,Ncols,3) ; plt.imshow(skpks[r:R,c:C])
        plt.title("Skel'd peaks",fontsize=ttlFontSize)
        plt.subplot(Nrows,Ncols,4) ; plt.imshow(epks[r:R,c:C])
        plt.title('Eroded peaks ; '+strElLabel[ErodElSize],fontsize=ttlFontSize)
        plt.subplot(Nrows,Ncols,5) ; plt.imshow(edpks[r:R,c:C])
        plt.title("Re-dilated peaks ; %ix"%(Ndil+1)+strElLabel[DilElSize],fontsize=ttlFontSize)
        plt.subplot(Nrows,Ncols,6) ; plt.imshow(fm[r:R,c:C])
        plt.title("Masked raw data",fontsize=ttlFontSize)
    return Npks

########################### Run analysis ######################################
###############################################################################

# Detect FFT peaks in hhoRing and count them.
# Outputs: counts == score per image & totCounts == score per position.

PSbase1='renamedPS/'  ; PSext='.ps.mrc' # Input power spectra ; path(,prefix) and extension
EMbase1='renamedFrames/' ; EMext='.mrc' # Input EM images ; path(,prefix) and extension
NnSpacer='_' # naming syntax : [Npos >>NnSpacer<< Nshot $extension]
Nmin= SEDFLAG1 # lowest pos# to consider in script. Edit it with parsePyScript.sh for parallel processing.
Nmax= SEDFLAG2 # highest "
tL=-20   # Hardcoded peak detection threshold (ignored if pcFlag=0)
pcFlag=0 # Prefer percentile of histogram for peax detection vs hard threshold ?
pcVal = 99.5    # Where to cut off histogram.  99.5% good for some 'semi-crystalline' positions but often too conservative crystalline ones
saveFigFlag=1 #save?
figOfolder='figures/allFrames-msk810_870-tL-20+EM/' #figOfldr
matOfolder='npOutput/' #countsOfolder
oMat=matOfolder+'allFrames-msk810_870-tL-20_counts_from%ito%i.npy'%(Nmin,Nmax) #countsOfile

#####################  Hardcoded for 5 shots per image.
counts=np.zeros((Nmax-Nmin+1,5))
cutoff=np.zeros((Nmax-Nmin+1,5))

for N in range(Nmin,Nmax+1):
    PSbase2=PSbase1+str(N)+NnSpacer ; EMbase2=EMbase1+str(N)+NnSpacer
    for n in (1,2,3,4,5):
        I=PSbase2+str(n)+PSext
        EMinput=EMbase2+str(n)+EMext
        fm,cutoff[N-Nmin,n-1]=hho_hist(I,pcVal)
        if saveFigFlag==1:
            plt.close()
        #print(cutoff)
        if pcFlag==1:
            Npks=hho_peaks(I,cutoff)
        else:
            Opath=figOfolder+str(N)+'_'+str(n)+'.png'
            Npks=hho_peaks(I,tL,tH=0,plotFlag=1,emFlag=1,EMinput=EMinput,saveFlag=saveFigFlag,Opath=Opath)   
        counts[N-Nmin,n-1]=Npks
totCounts=np.sum(counts,1)

#Save counts for iceQsummary.ipynb
with open(oMat,'wb') as f:
    np.save(f,counts)
