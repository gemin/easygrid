#!/bin/bash
#
# Script to rename frames to call them easily in following (python) scripts : Npos_Nshot.mrc
#
# will bug if you don't provide input list.
# if 5 images per position, you can use this command to generate the input list :
# awk '{print int((NR+4)/5),(NR-1)%5+1,$1}' frame.list > frame.renaming.list
#
set -e
set -o nounset 
#
renameList=$1 # pos# frame# frameOriName			# Input list ; 'how to rename which files'
iFolder='MotionCorr/job003/frames/'	# path to motion-corrected .mrc images
oFolder='renamedFrames/' # Opath (output path)
oSpacer='_' # Oname (output name) will be Npos $oSpacer Nimage $oExt(ension)
oExt='.mrc'
#
#########>> end of user input <<#########
#
while read i ; do   # surprising syntax to loop variable through text file - part 1

 N=`echo $i | awk '{print $1}'`     # quick fix to read matrices in shell ; 1st column
 n=`echo $i | awk '{print $2}'`     # idem ; 2nd col
 iName=`echo $i | awk '{print $3}'` # idem ; 3rd col
 oName=${N}${oSpacer}${n}$oExt      # define output file name
 ln -s ../${iFolder}$iName ${oFolder}$oName #link 

done <$renameList   # surprising syntax to loop variable through text file - part 2
