#!/bin/bash
#
# Script to replicate 2_iceQanalysisTemplate.py into parallelizable scripts.py analyzing subsets of positions.
#
# will bug if you don't provide input list.
# you can use the following commands to generate an input list with groups of 30 positions to process per script/job. You will need to trim that list according to your data size (unless you acquired images at exactly 3030 positions):
# echo "" > temp ; rm pyparse.list ; for i in `seq 0 100` ; do awk -v var="$i" '{print var*30+1,"\t",(var+1)*30}' temp >> pyparse.list ; done    # create input list
# rm temp ; vi pyparse.list  ## MANUAL STEP ## edit last number in list to be your actual N_max_positions
#
set -e
set -o nounset
#
parseList=$1 # begin# end#		# Input list ; 'how to split the N_pos,max in smaller subsets ?'
Ipy='2_iceQanalysisTemplate' # input .py name without extension
#
#########>> end of user input <<#########
#
while read i ; do   # surprising syntax to loop variable through text file - part 1

 iNi=`echo $i | awk '{print $1}'`		# quick fix to read matrices in shell ; 1st column
 eNd=`echo $i | awk '{print $2}'`		# idem ; 2nd col
 Opy="${Ipy}_from${iNi}to${eNd}.py"		# define output file name
 rsync -avz $Ipy.py $Opy 				#copy
 sed -i "s/SEDFLAG1/${iNi}/g" $Opy		#edit lower boundary
 sed -i "s/SEDFLAG2/${eNd}/g" $Opy		#edit higher boundary

done <$parseList    # surprising syntax to loop variable through text file - part 2
